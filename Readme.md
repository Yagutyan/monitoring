# Gas & Water Usage Monitoring Application

Project to record and monitor gas and water usage

# Requirements
- Java 8 JDK
- Maven
- HSQLDB

# Running

IDEs:
Import the project and add the run Application configuration if it does not already exist.

Maven:
`mvn spring-boot:run`

Explore:
swagger-ui: http://localhost:8080/swagger-ui.html

By default the project is configured to run at `http://localhost:8080`, but the port can be changed in `resources/application.properties`

