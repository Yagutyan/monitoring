package com.monitoring.monitoring.dto;

import lombok.Data;

import java.util.List;

@Data
public class ChargeHistoryReponseDto {
    public String firstName;
    public String secondName;
    public List<ChargeResponseDto> chargeDtoList;
}
