package com.monitoring.monitoring.dto;

import com.monitoring.monitoring.model.Charge;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChargeDto {
    @NotNull(message = "Please provide a customer id")
    public Long customerId;
    @NotNull(message = "Please provide a usage information")
    public Integer usage;
    @NotNull(message = "Please provide a type")
    public Charge.ChargeType chargeType;
}
