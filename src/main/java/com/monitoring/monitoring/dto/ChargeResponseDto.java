package com.monitoring.monitoring.dto;

import lombok.Data;

@Data
public class ChargeResponseDto {
    public String usage;
    public String chargeType;
}
