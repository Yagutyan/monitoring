package com.monitoring.monitoring.repository;

import com.monitoring.monitoring.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
