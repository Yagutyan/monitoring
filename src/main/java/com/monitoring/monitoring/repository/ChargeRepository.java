package com.monitoring.monitoring.repository;

import com.monitoring.monitoring.model.Charge;
import com.monitoring.monitoring.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChargeRepository extends JpaRepository<Charge, Long> {
    List<Charge> findByCustomer(Customer customer);
}
