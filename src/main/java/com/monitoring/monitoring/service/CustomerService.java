package com.monitoring.monitoring.service;

import com.monitoring.monitoring.dto.ChargeDto;
import com.monitoring.monitoring.dto.ChargeHistoryReponseDto;
import com.monitoring.monitoring.dto.ChargeResponseDto;
import com.monitoring.monitoring.model.Charge;
import com.monitoring.monitoring.model.Customer;
import com.monitoring.monitoring.model.exception.CustomerNotFoundException;
import com.monitoring.monitoring.repository.ChargeRepository;
import com.monitoring.monitoring.repository.CustomerRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final ChargeRepository chargeRepository;

    public CustomerService(CustomerRepository customerRepository, ChargeRepository chargeRepository) {
        this.customerRepository = customerRepository;
        this.chargeRepository = chargeRepository;
    }

    public ChargeHistoryReponseDto getCustomerHistory(Long customerId) {
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if (!customerOptional.isPresent()) {
            throw new CustomerNotFoundException();
        }
        Customer customer = customerOptional.get();
        List<Charge> charges = chargeRepository.findByCustomer(customer);

        ChargeHistoryReponseDto chargeHistoryDto = new ChargeHistoryReponseDto();
        chargeHistoryDto.setFirstName(customer.getFirstName());
        chargeHistoryDto.setSecondName(customer.getSecondName());

        List<ChargeResponseDto> chargeResponseDtos = charges.stream()
                .map(charge -> charge.toDto())
                .collect(Collectors.toList());

        chargeHistoryDto.setChargeDtoList(chargeResponseDtos);

        return chargeHistoryDto;
    }

    public void saveCharge(ChargeDto chargeDto) {
        Optional<Customer> customerOptional = customerRepository.findById(chargeDto.getCustomerId());
        if (!customerOptional.isPresent()) {
            throw new CustomerNotFoundException();
        }

        Charge charge = new Charge();
        charge.setCustomer(customerOptional.get());
        charge.setType(chargeDto.getChargeType());
        charge.setUsage(chargeDto.getUsage());
        chargeRepository.save(charge);
    }
}
