package com.monitoring.monitoring.controller;

import com.monitoring.monitoring.dto.ChargeDto;
import com.monitoring.monitoring.dto.ChargeHistoryReponseDto;
import com.monitoring.monitoring.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/history")
    public ResponseEntity<ChargeHistoryReponseDto> getCustomerHistory(@RequestParam Long customerId) {
        return new ResponseEntity<>(customerService.getCustomerHistory(customerId), HttpStatus.OK);
    }

    @PostMapping("/submitmeasurement")
    public ResponseEntity<Void> submitMeasurement(@Valid @RequestBody ChargeDto chargeDto) {
        customerService.saveCharge(chargeDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
