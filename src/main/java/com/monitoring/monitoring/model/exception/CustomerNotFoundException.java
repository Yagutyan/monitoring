package com.monitoring.monitoring.model.exception;

public class CustomerNotFoundException extends RuntimeException {

    public CustomerNotFoundException() {
        super("Customer could not be found.");
    }
}
