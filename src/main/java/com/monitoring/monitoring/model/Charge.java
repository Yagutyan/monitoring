package com.monitoring.monitoring.model;

import com.monitoring.monitoring.dto.ChargeResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Charge {
    public enum ChargeType {
        WATER, ELECTRICITY;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "usage_id", unique = true, nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @Column(name = "usage")
    private Integer usage;

    @Enumerated(EnumType.ORDINAL)
    private ChargeType type;

    public ChargeResponseDto toDto() {
        ChargeResponseDto chargeDto = new ChargeResponseDto();
        chargeDto.setChargeType(this.type.toString());
        chargeDto.setUsage(this.getUsage().toString());

        return chargeDto;
    }
}
