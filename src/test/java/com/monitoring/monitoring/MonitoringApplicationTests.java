package com.monitoring.monitoring;

import com.monitoring.monitoring.dto.ChargeHistoryReponseDto;
import com.monitoring.monitoring.model.Charge;
import com.monitoring.monitoring.model.Customer;
import com.monitoring.monitoring.repository.ChargeRepository;
import com.monitoring.monitoring.repository.CustomerRepository;
import com.monitoring.monitoring.service.CustomerService;
import com.sun.tools.javac.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MonitoringApplicationTests {

	@Mock
	CustomerRepository customerRepository;

	@Mock
	ChargeRepository chargeRepository;

	@InjectMocks
	CustomerService customerService;

	@Test
	public void find_customer_history() {
		Long customerId = 1L;

		when(customerRepository.findById(customerId)).thenReturn(Optional.of(new Customer(1L, "Patrich", "Smith")));
		when(chargeRepository.findByCustomer(any(Customer.class))).thenReturn(List.of(new Charge(1l, any(Customer.class), 200, Charge.ChargeType.WATER)));

		ChargeHistoryReponseDto chargeResponseDto = customerService.getCustomerHistory(1L);
		assertTrue(chargeResponseDto != null);
		assertTrue(chargeResponseDto.getChargeDtoList().size()==1);
	}

}
